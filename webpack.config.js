const path = require('path')
// const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCSSExtractPlugin = require('mini-css-extract-plugin')

const config = {
    mode: 'production',
    entry: path.resolve(__dirname, 'index.js'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module : {
        rules: [
            {
                test: /\.(scss|sass)$/,
                loader: [
                    MiniCSSExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {   
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            publicPath: '/dist/images',
                            outputPath: 'images'
                        }
                    }
                ]
            },
            {
                test: /\.(ttf|woff)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            publicPath: '/dist/fonts',
                            outputPath: 'fonts'
                        }
                    }
                ]
            },
            {
                type: 'javascript/auto',
                test: /\.json$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            publicPath: '/dist/json',
                            outputPath: 'json'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new MiniCSSExtractPlugin({
            filename: 'style.css'
        })
    ]
}

module.exports = config