'use strict';

module.exports = { 
    worldSize: {
        h: 3000,
        w: 4500
    },
    winSize: {
        h: window.innerHeight,
        w: window.innerWidth
    },
    jsonSprite: (_image, _json, _animated = false) => {
        let frames = _json.data.frames;
        let frameMeta = _json.data.meta;
        let frameKeys = Object.keys(frames);
        let arrayTextures = [];
        frameKeys.forEach((_value, _index) => {
          let frame = frames[_value];
          let rect = frame.frame;
          let resolution = parseInt(frameMeta.scale, 10);
         
          if(rect)
          {
            let texture = null;
            let size = null;
            let trim = null;
      
            if(frame.rotated)
            {
              size = new PIXI.Rectangle(rect.x, rect.y, rect.h, rect.w);
            }
            else{
              size = new PIXI.Rectangle(rect.x, rect.y, rect.w, rect.h);
            }
      
            if(frame.trimmed){
              trim = new PIXI.Rectangle(
                frame.spriteSourceSize.x / resolution,
                frame.spriteSourceSize.y / resolution,
                frame.sourceSize.w / resolution,
                frame.sourceSize.h / resolution
              );
            }
      
            if(frame.rotated){
              let temp = size.width;
              size.width = size.height;
              size.height = temp;
            }
      
            size.x /= resolution;
            size.y /= resolution;
            size.width /= resolution;
            size.height /= resolution;
      
            texture = new PIXI.Texture(_image.texture.baseTexture, size, size.clone(), trim, frame.rotated);
            if(_animated)
                arrayTextures.push(texture);
              else
              arrayTextures[_value] = texture;
          }
        });
        return arrayTextures;
    }
}