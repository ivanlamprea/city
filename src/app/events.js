'use strict';

const AttachDragTo = (function () {
    let winSize, worldSize, limitZoomIn, limitZoomOut, isFullScreen = false;
    let elem = document.getElementById('App');
    let _AttachDragTo = function (app, el) {
        this.app = app;
        this.container = el; 
        this.draggin = false;
        this.init();
    }
    _AttachDragTo.prototype = {
        onDragstart: function (ev) {
            this.draggin = true;
            this.origin_x = ev.data.originalEvent.clientX || ev.data.global.x;
            this.origin_y = ev.data.originalEvent.clientY || ev.data.global.y;
        },
        onDragmove: function (ev) {
            if(this.draggin) {
                let smap4 = this.container.children[3];
                let tg = this.container,//ev.target,
                    x = ev.data.originalEvent.clientX || ev.data.global.x,
                    y = ev.data.originalEvent.clientY || ev.data.global.y;

                let move_x = x - this.origin_x + this.origin_container_pos_x,
                    move_y = y - this.origin_y + this.origin_container_pos_y;
                
                let cw = [smap4.width + smap4.x] * this.factor_scale,
                    ch = [smap4.height + smap4.y] * this.factor_scale;
            
                let limit_x = winSize.w - cw,
                    limit_y = winSize.h - ch;

                if(move_x < 0 && move_x > limit_x)
                    tg.x = move_x || 0;
                if(move_y < 0 && move_y > limit_y)
                    tg.y = move_y || 0 ;  
            } 
            else {
                return;
            }
        },
        onDragend: function (ev) {
            this.draggin = false;
            this.origin_container_pos_x = this.container.x;
            this.origin_container_pos_y = this.container.y;
        },
        onResize: function() {
            // Set sizes of navigator in the object
            winSize.h = window.innerHeight;
            winSize.w = window.innerWidth;
       
            //This are break points for scale the world and center it.

            if(winSize.w > 1366)
            {

                this.factor_scale = 0.7;
                this.original_scale = 0.7;
                this.container.scale.set(0.7);
                this.container.x = [winSize.w - (worldSize.w * 0.7)] / 2;
                this.container.y = [winSize.w - (worldSize.w * 0.7)] / 2;
                this.origin_container_pos_x = this.container.x;
                this.origin_container_pos_y = this.container.y;
                
            }
            if(winSize.w <= 1366 && winSize.w > 768)
            {
                this.factor_scale = 0.6;
                this.original_scale = 0.6;
                this.container.scale.set(0.6);
                this.container.x = [winSize.w - (worldSize.w * 0.6)] / 2;
                this.container.y = [winSize.h - (worldSize.h * 0.6)] / 2;
                this.origin_container_pos_x = this.container.x;
                this.origin_container_pos_y = this.container.y;

            }
            else if(winSize.w <= 768)
            {
                this.factor_scale = 0.4;
                this.original_scale = 0.4;
                this.container.scale.set(0.4);
                this.container.x = [winSize.w - (worldSize.w * 0.5)] / 2;
                this.container.y = [winSize.h - (worldSize.h * 0.5)] / 2;
                this.origin_container_pos_x = this.container.x;
                this.origin_container_pos_y = this.container.y;

            }
            this.app.renderer.resize(window.innerWidth, window.innerHeight);
        },
        onZoomIn: function(){
            limitZoomIn = this.original_scale + 0.3;
            winSize.h = window.innerHeight;
            winSize.w = window.innerWidth;
            if(this.factor_scale < limitZoomIn){
                this.factor_scale += 0.05;
            }
            else{
                this.factor_scale = limitZoomIn;
            }
            this.container.scale.set(this.factor_scale);
            this.container.x = [winSize.w - (worldSize.w * this.factor_scale)] / 2;
            this.container.y = [winSize.h - (worldSize.h * this.factor_scale)] / 2;
            this.origin_container_pos_x = this.container.x;
            this.origin_container_pos_y = this.container.y;
            this.app.renderer.resize(window.innerWidth, window.innerHeight);
        },
        onZoomOut: function(){
            limitZoomOut = this.original_scale - 0.25;
            winSize.h = window.innerHeight;
            winSize.w = window.innerWidth;
            
            if(this.factor_scale > limitZoomOut )
            {
                this.factor_scale -= 0.05;
            }
            else{
                this.factor_scale = limitZoomOut
            }

            this.container.scale.set(this.factor_scale);
            this.container.x = [winSize.w - (worldSize.w * this.factor_scale)] / 2;
            this.container.y = [winSize.h - (worldSize.h * this.factor_scale)] / 2;
            this.origin_container_pos_x = this.container.x;
            this.origin_container_pos_y = this.container.y;
            this.app.renderer.resize(window.innerWidth, window.innerHeight);
        },
        onFullScreen: function(){
            if(!isFullScreen)
            {
                isFullScreen = true;
                if (elem.requestFullscreen) {
                    elem.requestFullscreen();
                } else if (elem.mozRequestFullScreen) {
                    elem.mozRequestFullScreen();
                } else if (elem.webkitRequestFullscreen) { 
                    elem.webkitRequestFullscreen();
                } else if (elem.msRequestFullscreen) { 
                    elem.msRequestFullscreen();
                }
                document.querySelector('#BtnFullScreen div').classList.remove('btn-expand');
                document.querySelector('#BtnFullScreen div').classList.add('btn-expand-out');
            }
            else{
                isFullScreen = false;
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.mozCancelFullScreen) { /* Firefox */
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
                    document.webkitExitFullscreen();
                } else if (document.msExitFullscreen) { /* IE/Edge */
                    document.msExitFullscreen();
                }
                document.querySelector('#BtnFullScreen div').classList.remove('btn-expand-out');
                document.querySelector('#BtnFullScreen div').classList.add('btn-expand');
            }
        },
        onClosepopup: function() {
            document.getElementById('Map').classList.remove('filter-blur');
            document.querySelector('.content-popups').style.display = 'none';
            document.querySelectorAll('.pop-up').forEach((_el) => {
                _el.style.display = 'none';
            });

            document.querySelectorAll('video').forEach((_vid) => {
                _vid.pause();
                _vid.currentTime = 0;
            });
        },
        init: function () {
            this.onResize();
            this.origin_container_pos_x = this.container.x;
            this.origin_container_pos_y = this.container.y;
            this.container.on('pointerdown', this.onDragstart.bind(this), false);
            this.container.on('pointermove', this.onDragmove.bind(this), false);
            this.container.on('pointerup', this.onDragend.bind(this), false);
            this.container.on('pointerupoutside', this.onDragend.bind(this), false);
            document.getElementById('BtnZoomIn').addEventListener('click', this.onZoomIn.bind(this), false);
            document.getElementById('BtnZoomOut').addEventListener('click', this.onZoomOut.bind(this), false);
            document.getElementById('BtnFullScreen').addEventListener('click', this.onFullScreen.bind(this), false);
            document.querySelectorAll('.btn-close').forEach((_el) => {
                _el.addEventListener('click', this.onClosepopup.bind(this), false);                
            });
            window.addEventListener('resize', this.onResize.bind(this),false);
        
        }
    }
    return function(_app, _el, _winSize, _worldSize) {
        winSize = _winSize;
        worldSize = _worldSize;
        return new _AttachDragTo(_app, _el);
    }
})();

module.exports = AttachDragTo;