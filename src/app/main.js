'use strict'
import {winSize, worldSize, jsonSprite} from './config';
import AttachDragTo from'./events';
import { getCars, animatedVehicles } from './vehicles';

import import_map1 from '../images/background/m1.jpg';
import import_map2 from '../images/background/m2.jpg';
import import_map3 from '../images/background/m3.jpg';
import import_map4 from '../images/background/m4.jpg';
import import_s26_1a from '../images/background/s26_1a.png';
import import_s26_1b from '../images/background/s26_1b.png';
import import_s26_2a from '../images/background/s26_2a.png';
import import_s26_2b from '../images/background/s26_2b.png';
import import_s26_3a from '../images/background/s26_3a.png';
import import_s26_3b from '../images/background/s26_3b.png';
import import_s26_4a from '../images/background/s26_4a.png';
import import_s26_4b from '../images/background/s26_4b.png';
import import_bg_car1 from '../images/background/bg_car1.png';
import import_bg_car2 from '../images/background/bg_car2.png';
import import_bg_car3 from '../images/background/bg_car3.png';
import import_bg_car4 from '../images/background/bg_car4.png';
import import_build1 from '../images/background/b1.png';
import import_build2 from '../images/background/b2.png';
import import_build3 from '../images/background/b3.png';
import import_build4 from '../images/background/b4.png';
import import_object1 from '../images/background/obj1.png';
import import_object2 from '../images/background/obj2.png';
import import_object3 from '../images/background/obj3.png';
import import_object4 from '../images/background/obj4.png';

//LoadSprites
import import_vehicles_png from '../images/sprites/vehicles.png';
import import_vehicles_json from '../images/sprites/vehicles.json';

import import_buildings_png from '../images/sprites/buildings.png';
import import_buildings_json from '../images/sprites/buildings.json';

import import_tags_png from '../images/sprites/tags.png';
import import_tags_json from '../images/sprites/tags.json';

import import_fruit_png from '../images/sprites/fruit.png';
import import_fruit_json from '../images/sprites/fruit.json';

import import_ghost_png from '../images/sprites/ghost.png';
import import_ghost_json from '../images/sprites/ghost.json';

import import_hammer_png from '../images/sprites/hammer.png';
import import_hammer_json from '../images/sprites/hammer.json';

import import_heart_png from '../images/sprites/heart.png';
import import_heart_json from '../images/sprites/heart.json';

import import_king_png from '../images/sprites/king.png';
import import_king_json from '../images/sprites/king.json';


const app = new PIXI.Application(winSize.w, winSize.h);
const container = new PIXI.Container();
const loader = new PIXI.Loader();

let isLoader= false, preloadCounter = 0;
let map1, map2, map3, map4, s26_1a, s26_1b, s26_2a, s26_2b, s26_3a, s26_3b, s26_4a, s26_4b, bg_car1, bg_car2, bg_car3, bg_car4, build1, build2, build3, build4, object1, object2, object3, object4;
let carGroup1 = [], carGroup2 = [], carGroup3 = [], carGroup4 = [], train1, train2;
let roll_auditoria, roll_biblioteca, roll_colegio, roll_construccion, roll_contraloria, roll_gobernacion;
let tag_auditoria, tag_biblioteca, tag_colegio, tag_construccion, tag_contraloria, tag_gobernacion, tag_hueco1, tag_hueco2, tag_hueco3, tag_tren;

app.renderer.plugins.interaction.autoPreventDefault = false;
app.renderer.autoDensity = true;
document.getElementById('Map').appendChild(app.view);

container.interactive = true;
container.interactiveChildren = true;
container.height = worldSize.h;
container.width = worldSize.w;

loader.add('map1', import_map1)
      .add('map2', import_map2)
      .add('map3', import_map3)
      .add('map4', import_map4)
      .add('s26_1a', import_s26_1a)
      .add('s26_1b', import_s26_1b)
      .add('s26_2a', import_s26_2a)
      .add('s26_2b', import_s26_2b)
      .add('s26_3a', import_s26_3a)
      .add('s26_3b', import_s26_3b)
      .add('s26_4a', import_s26_4a)
      .add('s26_4b', import_s26_4b)
      .add('bg_car1', import_bg_car1)
      .add('bg_car2', import_bg_car2)
      .add('bg_car3', import_bg_car3)
      .add('bg_car4', import_bg_car4)
      .add('build1', import_build1)
      .add('build2', import_build2)
      .add('build3', import_build3)
      .add('build4', import_build4)
      .add('object1', import_object1)
      .add('object2', import_object2)
      .add('object3', import_object3)
      .add('object4', import_object4)
      .add('sp_vehicles_png', import_vehicles_png)
      .add('sp_vehicles_json', import_vehicles_json)
      .add('sp_buildings_png', import_buildings_png)
      .add('sp_buildings_json', import_buildings_json)
      
      .add('sp_fruit_png', import_fruit_png)
      .add('sp_fruit_json', import_fruit_json)

      .add('sp_ghost_png', import_ghost_png)
      .add('sp_ghost_json', import_ghost_json)

      .add('sp_hammer_png', import_hammer_png)
      .add('sp_hammer_json', import_hammer_json)

      .add('sp_heart_png', import_heart_png)
      .add('sp_heart_json', import_heart_json)

      .add('sp_king_png', import_king_png)
      .add('sp_king_json', import_king_json)
      
      .add('sp_tags_png', import_tags_png)
      .add('sp_tags_json', import_tags_json);



loader.size = function(){
    let size = 0, key;
    for (key in this.resources) {
        if (this.resources.hasOwnProperty(key)) size++;
    }
    return size;
}

loader.load((loader, resources) => {
  
    let vehicles = jsonSprite(resources.sp_vehicles_png, resources.sp_vehicles_json),
        buildings = jsonSprite(resources.sp_buildings_png, resources.sp_buildings_json),
        tags = jsonSprite(resources.sp_tags_png, resources.sp_tags_json),
        fruit = jsonSprite(resources.sp_fruit_png, resources.sp_fruit_json, true),
        ghost = jsonSprite(resources.sp_ghost_png, resources.sp_ghost_json, true),
        hammer = jsonSprite(resources.sp_hammer_png, resources.sp_hammer_json, true),
        heart = jsonSprite(resources.sp_heart_png, resources.sp_heart_json, true),
        king = jsonSprite(resources.sp_king_png, resources.sp_king_json, true);
    

    let fruitAnimated = new  PIXI.AnimatedSprite(fruit),
        ghostAnimated = new PIXI.AnimatedSprite(ghost),
        hammerAnimated = new PIXI.AnimatedSprite(hammer),
        heartAnimated = new PIXI.AnimatedSprite(heart),
        kingAnimated = new PIXI.AnimatedSprite(king);
    
    let btn_auditoria = new PIXI.Graphics(),
        btn_biblioteca = new PIXI.Graphics(),
        btn_colegio = new PIXI.Graphics(),
        btn_construccion = new PIXI.Graphics(),
        btn_contraloria = new PIXI.Graphics(),
        btn_gobernacion = new PIXI.Graphics(),
        btn_hueco1 = new PIXI.Graphics(),
        btn_hueco2 = new PIXI.Graphics(),
        btn_hueco3 = new PIXI.Graphics(),
        btn_tren = new PIXI.Graphics();

    map1 = new PIXI.Sprite(resources.map1.texture),
    map2 = new PIXI.Sprite(resources.map2.texture),
    map3 = new PIXI.Sprite(resources.map3.texture),
    map4 = new PIXI.Sprite(resources.map4.texture);
    s26_1a = new PIXI.Sprite(resources.s26_1a.texture);
    s26_1b = new PIXI.Sprite(resources.s26_1b.texture);
    s26_2a = new PIXI.Sprite(resources.s26_2a.texture);
    s26_2b = new PIXI.Sprite(resources.s26_2b.texture);
    s26_3a = new PIXI.Sprite(resources.s26_3a.texture);
    s26_3b = new PIXI.Sprite(resources.s26_3b.texture);
    s26_4a = new PIXI.Sprite(resources.s26_4a.texture);
    s26_4b = new PIXI.Sprite(resources.s26_4b.texture);

    bg_car1 = new PIXI.Sprite(resources.bg_car1.texture);
    bg_car2 = new PIXI.Sprite(resources.bg_car2.texture);
    bg_car3 = new PIXI.Sprite(resources.bg_car3.texture);
    bg_car4 = new PIXI.Sprite(resources.bg_car4.texture);

    build1 = new PIXI.Sprite(resources.build1.texture);
    build2 = new PIXI.Sprite(resources.build2.texture);
    build3 = new PIXI.Sprite(resources.build3.texture);
    build4 = new PIXI.Sprite(resources.build4.texture);

    object1 = new PIXI.Sprite(resources.object1.texture);
    object2 = new PIXI.Sprite(resources.object2.texture);
    object3 = new PIXI.Sprite(resources.object3.texture);
    object4 = new PIXI.Sprite(resources.object4.texture);

    train1 = new PIXI.Sprite(vehicles['train1.png']);
    train2 = new PIXI.Sprite(vehicles['train2.png']);
   
    carGroup1 = getCars(vehicles, 'tr', 'road1', 18);//18
    carGroup2 = getCars(vehicles, 'tr', 'road2', 29);//29
    carGroup3 = getCars(vehicles, 'bl', 'road3', 24);//24
    carGroup4 = getCars(vehicles, 'bl', 'road4', 12);//12

    roll_auditoria = new PIXI.Sprite(buildings['roll_auditoria.png']);
    roll_biblioteca = new PIXI.Sprite(buildings['roll_biblioteca.png']);
    roll_colegio = new PIXI.Sprite(buildings['roll_colegio.png']);
    roll_construccion = new PIXI.Sprite(buildings['roll_construccion.png']);
    roll_contraloria = new PIXI.Sprite(buildings['roll_contraloria.png']);
    roll_gobernacion = new PIXI.Sprite(buildings['roll_gobernacion.png']);
   
    tag_auditoria = new PIXI.Sprite(tags['tag_auditoria.png']);
    tag_biblioteca = new PIXI.Sprite(tags['tag_biblioteca.png']);
    tag_colegio = new PIXI.Sprite(tags['tag_colegio.png']);
    tag_construccion = new PIXI.Sprite(tags['tag_construccion.png']);
    tag_contraloria = new PIXI.Sprite(tags['tag_contraloria.png']);
    tag_gobernacion = new PIXI.Sprite(tags['tag_gobernacion.png']);
    tag_hueco1 = new PIXI.Sprite(tags['tag_hueco.png']);
    tag_hueco2 = new PIXI.Sprite(tags['tag_hueco.png']);
    tag_hueco3 = new PIXI.Sprite(tags['tag_hueco.png']);
    tag_tren = new PIXI.Sprite(tags['tag_tren.png']);
    
    map1.x = 0;
    map1.y = 0;
    map2.x = worldSize.w / 2;
    map2.y = 0;
    map3.x = 0;
    map3.y = worldSize.h / 2;
    map4.x = worldSize.w / 2;
    map4.y = worldSize.h / 2;

    bg_car1.x = 0;
    bg_car1.y = 0;
    bg_car2.x = worldSize.w / 2;
    bg_car2.y = 0;
    bg_car3.x = 0;
    bg_car3.y = worldSize.h / 2;
    bg_car4.x = (worldSize.w / 2) - 110;
    bg_car4.y = worldSize.h / 2;

    s26_1a.x = 0;
    s26_1a.y = 1614;
    s26_1b.x = 2250;
    s26_1b.y = 394;
    s26_2a.x = 0;
    s26_2a.y = 1785;
    s26_2b.x = 2250;
    s26_2b.y = 504;
    s26_3a.x = 622;
    s26_3a.y = 2006;
    s26_3b.x = 2250;
    s26_3b.y = 753;
    s26_4a.x = 1072;
    s26_4a.y = 2019;
    s26_4b.x = 2746;
    s26_4b.y = 1007;

    build1.x = 0;
    build1.y = 0;
    build2.x = worldSize.w / 2;
    build2.y = 0;
    build3.x = 0;
    build3.y = worldSize.h / 2;
    build4.x = worldSize.w / 2;
    build4.y = worldSize.h / 2;

    object1.x = 0;
    object1.y = 0;
    object2.x = worldSize.w / 2;
    object2.y = 0;
    object3.x = 0;
    object3.y = worldSize.h / 2;
    object4.x = worldSize.w / 2;
    object4.y = worldSize.h / 2;

    fruitAnimated.play();
    fruitAnimated.x = 1513;
    fruitAnimated.y = 2597;
    fruitAnimated.animationSpeed = 0.2;

    ghostAnimated.play();
    ghostAnimated.x = 3277;
    ghostAnimated.y = 1376;
    ghostAnimated.animationSpeed = 0.2;

    hammerAnimated.play();
    hammerAnimated.x = 2944;
    hammerAnimated.y = 1519;
    hammerAnimated.animationSpeed = 0.2;

    heartAnimated.play();
    heartAnimated.x = 1916;
    heartAnimated.y = 1775;
    heartAnimated.animationSpeed = 0.2;

    kingAnimated.play();
    kingAnimated.x = 936;
    kingAnimated.y = 2706;
    kingAnimated.animationSpeed = 0.2;
        
    train1.origin = { x: 4473, y: -117 }
    train1.x = train1.origin.x;
    train1.y = train1.origin.y;
    train1.speed = 4;
    train1.randomSpeed = false;
    train1.direction = Math.PI / 6;
    
    train2.x = 2775;
    train2.y = 1003;
    tag_tren.x = 3394;
    tag_tren.y = 1253;
    tag_tren.alpha = 0;   
    btn_tren.interactive = true;
    btn_tren.cursor = 'pointer';
    btn_tren.beginFill(0x000000, 0.001);
    btn_tren.lineStyle(1, 0x00000, 0);
    btn_tren.moveTo(2699, 1818);
    btn_tren.lineTo(2736, 1835);
    btn_tren.lineTo(4014, 1091);
    btn_tren.lineTo(4000, 1039);
    btn_tren.lineTo(2703, 1788)
    btn_tren.closePath();
    btn_tren.endFill();
    btn_tren.on('pointerover', () => { tag_tren.alpha = 1; });
    btn_tren.on('pointerout', () => { tag_tren.alpha = 0; });
    btn_tren.on('pointertap', ()=> { tag_tren.alpha = 1;  console.log('click'); });

    roll_auditoria.x = 2434;
    roll_auditoria.y = 1905;
    roll_auditoria.alpha = 0;
    tag_auditoria.x = 2910;
    tag_auditoria.y = 1930;
    tag_auditoria.alpha = 0;
    btn_auditoria.interactive = true;
    btn_auditoria.cursor = 'pointer';
    btn_auditoria.beginFill(0x000000, 0.001);
    btn_auditoria.lineStyle(1, 0x00000, 0);
    btn_auditoria.moveTo(2473, 2583);
    btn_auditoria.lineTo(2532, 2622);
    btn_auditoria.lineTo(2954, 2387);
    btn_auditoria.lineTo(2959, 2204);
    btn_auditoria.lineTo(3351, 2449);
    btn_auditoria.lineTo(3433, 2408);
    btn_auditoria.lineTo(3434, 2190);
    btn_auditoria.lineTo(2981, 1930);
    btn_auditoria.lineTo(2907, 1971);
    btn_auditoria.lineTo(2907, 2061);
    btn_auditoria.lineTo(2661, 2193);
    btn_auditoria.lineTo(2663, 2004);
    btn_auditoria.lineTo(2473, 2081);
    btn_auditoria.closePath();
    btn_auditoria.endFill();
    btn_auditoria.on('pointerover', () => { roll_auditoria.alpha = 1; tag_auditoria.alpha = 1; });
    btn_auditoria.on('pointerout', () => { roll_auditoria.alpha = 0; tag_auditoria.alpha = 0; });
    btn_auditoria.on('pointertap', ()=> {
        roll_auditoria.alpha = 1;
        roll_auditoria.alpha = 1;
        document.getElementById('Map').classList.add('filter-blur'); 
        document.querySelector('.content-popups').style.display = 'flex';
        document.getElementById('Auditoria').style.display = 'block';
        document.getElementById('BtnAuditoria').style.display = 'block';
    });

    roll_biblioteca.x = 355;
    roll_biblioteca.y = 656;
    roll_biblioteca.alpha = 0;
    tag_biblioteca.x = 1266;
    tag_biblioteca.y = 992;
    tag_biblioteca.alpha = 0;
    btn_biblioteca.interactive = 1;
    btn_biblioteca.cursor = 'pointer';
    btn_biblioteca.lineStyle(1, 0x000000, 0);
    btn_biblioteca.beginFill(0x000000, 0.001);
    btn_biblioteca.drawEllipse(957, 1092, 558, 310);
    btn_biblioteca.endFill();
    btn_biblioteca.on('pointerover', () => { roll_biblioteca.alpha = 1; tag_biblioteca.alpha = 1; });
    btn_biblioteca.on('pointerout', () => { roll_biblioteca.alpha = 0; tag_biblioteca.alpha = 0; });
    btn_biblioteca.on('pointertap', ()=> { 
        btn_biblioteca.alpha = 1;
        btn_biblioteca.alpha = 1;
        document.getElementById('Map').classList.add('filter-blur');
        document.querySelector('.content-popups').style.display = 'flex';
        document.getElementById('Biblioteca').style.display = 'block';
        document.getElementById('BtnBiblioteca').style.display = 'block';
    });

    roll_colegio.x = 2393;
    roll_colegio.y = 186;
    roll_colegio.alpha = 0;
    tag_colegio.x = 2880;
    tag_colegio.y = 119;
    tag_colegio.alpha = 0;
    btn_colegio.interactive = 1;
    btn_colegio.cursor = 'pointer';
    btn_colegio.beginFill(0x000000, 0.001);
    btn_colegio.lineStyle(1, 0x00000, 0);
    btn_colegio.moveTo(2408, 509);
    btn_colegio.lineTo(2848, 767);
    btn_colegio.lineTo(2975, 694);
    btn_colegio.lineTo(2975, 668);
    btn_colegio.lineTo(3058, 615);
    btn_colegio.lineTo(3056, 962);
    btn_colegio.lineTo(3174, 396);
    btn_colegio.lineTo(2889, 231);
    btn_colegio.closePath();
    btn_colegio.endFill();
    btn_colegio.on('pointerover', () => { roll_colegio.alpha = 1; tag_colegio.alpha = 1; });
    btn_colegio.on('pointerout', () => { roll_colegio.alpha = 0; tag_colegio.alpha = 0; });
    btn_colegio.on('pointertap', ()=> { 
        roll_colegio.alpha = 1;
        tag_colegio.alpha = 1;
        document.getElementById('Map').classList.add('filter-blur');
        document.querySelector('.content-popups').style.display = 'flex';
        document.getElementById('Colegio').style.display = 'block';
        document.getElementById('BtnColegio').style.display = 'block';
    });

    roll_construccion.x = 28;
    roll_construccion.y = 2213;
    roll_construccion.alpha = 0;
    tag_construccion.x = 348;
    tag_construccion.y = 2107;
    tag_construccion.alpha = 0;
    btn_construccion.interactive = true;
    btn_construccion.cursor = 'pointer';
    btn_construccion.beginFill(0x000000, 0.001);
    btn_construccion.lineStyle(1, 0x000000, 0);
    btn_construccion.moveTo(55, 2628);
    btn_construccion.lineTo(134, 2674);
    btn_construccion.lineTo(370, 2530);
    btn_construccion.lineTo(373, 2242);
    btn_construccion.lineTo(255, 2243);
    btn_construccion.lineTo(255, 2365);
    btn_construccion.lineTo(55, 2476);
    btn_construccion.closePath();
    btn_construccion.endFill();
    btn_construccion.on('pointerover', () => { roll_construccion.alpha = 1; tag_construccion.alpha = 1; });
    btn_construccion.on('pointerout', () => { roll_construccion.alpha = 0; tag_construccion.alpha = 0; });
    btn_construccion.on('pointertap', () => { 
        btn_construccion.alpha = 1;
        btn_construccion.alpha = 1;
        document.getElementById('Map').classList.add('filter-blur'); 
        document.querySelector('.content-popups').style.display = 'flex';
        document.getElementById('Construccion').style.display = 'block';
    });

    roll_contraloria.x = 725;
    roll_contraloria.y = 1742;
    roll_contraloria.alpha = 0;
    tag_contraloria.x = 1133;
    tag_contraloria.y = 1790;
    tag_contraloria.alpha = 0;
    btn_contraloria.interactive = true;
    btn_contraloria.cursor = 'pointer';
    btn_contraloria.beginFill(0x000000, 0.001);
    btn_contraloria.lineStyle(1, 0x00000, 0);
    btn_contraloria.moveTo(748, 2014);
    btn_contraloria.lineTo(775, 2030);
    btn_contraloria.lineTo(904, 1964);
    btn_contraloria.lineTo(964, 2000);
    btn_contraloria.lineTo(965, 2150);
    btn_contraloria.lineTo(1118, 2237);
    btn_contraloria.lineTo(1288, 2138);
    btn_contraloria.lineTo(1130, 2050);
    btn_contraloria.lineTo(1175, 1901);
    btn_contraloria.lineTo(942, 1768);
    btn_contraloria.lineTo(829, 1830);
    btn_contraloria.lineTo(830, 1968);
    btn_contraloria.closePath();
    btn_contraloria.endFill();
    btn_contraloria.on('pointerover', () => { roll_contraloria.alpha = 1; tag_contraloria.alpha = 1; });
    btn_contraloria.on('pointerout', () => { roll_contraloria.alpha = 0; tag_contraloria.alpha = 0; });
    btn_contraloria.on('pointertap', () => { 
        btn_contraloria.alpha = 1;
        btn_contraloria.alpha = 1;
        document.getElementById('Map').classList.add('filter-blur');
        document.querySelector('.content-popups').style.display = 'flex';
        document.getElementById('Contraloria').style.display = 'block';
        document.getElementById('BtnContraloria').style.display = 'block';
    });

   
    roll_gobernacion.x = 3683;
    roll_gobernacion.y = 1425;
    roll_gobernacion.alpha = 0;
    tag_gobernacion.x = 4042;
    tag_gobernacion.y = 1465;
    tag_gobernacion.alpha = 0;
    btn_gobernacion.interactive = true;
    btn_gobernacion.cursor = 'pointer';
    btn_gobernacion.beginFill(0x000000, 0.001);
    btn_gobernacion.lineStyle(1, 0x00000, 0);
    btn_gobernacion.moveTo(3659, 1731);
    btn_gobernacion.lineTo(4144, 2041);
    btn_gobernacion.lineTo(4500, 1864);
    btn_gobernacion.lineTo(4500, 1566);
    btn_gobernacion.lineTo(4217, 1406);
    btn_gobernacion.closePath();
    btn_gobernacion.endFill();
    btn_gobernacion.on('pointerover', () => { roll_gobernacion.alpha = 1; tag_gobernacion.alpha = 1; });
    btn_gobernacion.on('pointerout', () => { roll_gobernacion.alpha = 0; tag_gobernacion.alpha = 0; });
    btn_gobernacion.on('pointertap', ()=> { 
        btn_gobernacion.alpha = 1;
        btn_gobernacion.alpha = 1;
        document.getElementById('Map').classList.add('filter-blur');
        document.querySelector('.content-popups').style.display = 'flex';
        document.getElementById('Gobernacion').style.display = 'block';
        document.getElementById('BtnGobernacion').style.display = 'block';
    });
    
    tag_hueco1.x = 789;
    tag_hueco1.y = 84;
    tag_hueco1.alpha = 0;
    btn_hueco1.interactive = true;
    btn_hueco1.cursor = 'pointer';
    btn_hueco1.beginFill(0x000000, 0.001);
    btn_hueco1.drawRect(801, 230, 64, 37);
    btn_hueco1.endFill();
    btn_hueco1.on('pointerover', () => { tag_hueco1.alpha = 1; });
    btn_hueco1.on('pointerout', () => { tag_hueco1.alpha = 0; });
    btn_hueco1.on('pointertap', ()=> { tag_hueco1.alpha = 1; console.log('click'); });

    tag_hueco2.x = 623;
    tag_hueco2.y = 1517;
    tag_hueco2.alpha = 0;
    btn_hueco2.interactive = true;
    btn_hueco2.cursor = 'pointer';
    btn_hueco2.beginFill(0x000000, 0.001);
    btn_hueco2.drawRect(640, 1667, 61, 33);
    btn_hueco2.endFill();
    btn_hueco2.on('pointerover', () => { tag_hueco2.alpha = 1; });
    btn_hueco2.on('pointerout', () => { tag_hueco2.alpha = 0; });
    btn_hueco2.on('pointertap', ()=> { tag_hueco2.alpha = 1; console.log('click'); });

    tag_hueco3.x = 2828;
    tag_hueco3.y = 2712;
    tag_hueco3.alpha = 0;
    btn_hueco3.interactive = true;
    btn_hueco3.cursor = 'pointer';
    btn_hueco3.beginFill(0x000000, 0.001);
    btn_hueco3.drawRect(2851, 2863, 59, 31);
    btn_hueco3.endFill();
    btn_hueco3.on('pointerover', () => { tag_hueco3.alpha = 1; });
    btn_hueco3.on('pointerout', () => { tag_hueco3.alpha = 0; });
    btn_hueco3.on('pointertap', ()=> { tag_hueco3.alpha = 1; console.log('click'); });

    container.addChild(map1, map2, map3, map4);
    container.addChild(bg_car1, bg_car2, bg_car3, bg_car4);
    carGroup1.forEach((_car)=> { container.addChild(_car); });
    container.addChild(s26_1a, s26_1b);
    carGroup2.forEach((_car)=> { container.addChild(_car); });
    container.addChild(s26_2a, s26_2b);
    container.addChild(train1, train2);
    container.addChild(s26_3a, s26_3b);
    carGroup3.forEach((_car)=> { container.addChild(_car); });
    container.addChild(s26_4a, s26_4b);
    carGroup4.forEach((_car)=> { container.addChild(_car); });
    container.addChild(build1, build2, build3, build4);
    
    container.addChild(fruitAnimated, ghostAnimated, hammerAnimated, heartAnimated, kingAnimated);

    container.addChild(roll_auditoria, roll_biblioteca, roll_colegio, roll_construccion, roll_contraloria, roll_gobernacion);
    container.addChild(tag_auditoria, tag_biblioteca, tag_colegio, tag_construccion, tag_contraloria, tag_gobernacion, tag_hueco1, tag_hueco2, tag_hueco3, tag_tren);
    container.addChild(object1, object2, object3, object4);
    container.addChild(btn_tren, btn_auditoria, btn_biblioteca, btn_colegio, btn_construccion, btn_contraloria, btn_gobernacion, btn_hueco1, btn_hueco2, btn_hueco3);
    
})
loader.onProgress.add((loader, resource) => {
    let total = loader.size();
    let complete = 0;
    preloadCounter++;
    complete = Math.round([100/total] * preloadCounter);
    document.getElementById('PreloadCounter').innerText = complete;
    if(complete === 100)
    {
        setTimeout(()=> {
            isLoader = true; 
            document.getElementById('Preload').style.display = 'none';
            //Start all animations
        },2000);
    }
});

app.stage.addChild(container);

AttachDragTo(app,container, winSize, worldSize);
app.ticker.add(() => {
    if(isLoader){ 
        animatedVehicles(train1, 'tr', worldSize);
        carGroup1.forEach((_car)=> {  animatedVehicles(_car, 'tr', worldSize); });
        carGroup2.forEach((_car)=> {  animatedVehicles(_car, 'tr', worldSize); });
        carGroup3.forEach((_car)=> {  animatedVehicles(_car, 'bl', worldSize); });
        carGroup4.forEach((_car)=> {  animatedVehicles(_car, 'bl', worldSize); });
    }
});
