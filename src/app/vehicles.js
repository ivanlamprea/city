'use strict';

let roads = {
    road1: [{ x: 4444, y: 265, speed: 1.2 }, { x: 4449, y: 291, speed: 1.4 } ],
    road2: [{ x: 4451, y: 424, speed: 1.3 }, { x: 4455, y: 449, speed: 1.5 }, { x: 4455, y: 476, speed: 1.7 }, { x: 4449, y: 504, speed: 1.9 }],
    road3: [{ x: 843, y: 2934, speed: 1.9 }, { x: 868, y: 2947, speed: 1.7 }, { x: 916, y: 2945, speed: 1.5 }, { x: 960, y: 2947, speed: 1.3 }],
    road4: [{ x: 1190, y: 2948, speed: 1.4 }, { x: 1246, y: 2944, speed: 1.2 }]
};

let typeCar = { tr: 15, bl: 15};

function getCars (_sprite, _orientation, _road, _cant) {
    let tmpCars = [];
    let orderRails = [[],[],[],[]];
    let tmpCar = null;
    let cantRail = roads[_road].length;
    
    for(let i = 0; i < _cant; i++)
    {
        let randomRail = Math.floor(Math.random() * cantRail) + 0; 
        let randomCar = Math.floor(Math.random() * typeCar[_orientation]) + 1;
        let rail = roads[_road][randomRail];

        let position = orderRails[randomRail].length * 450;
        
        tmpCar = new PIXI.Sprite(_sprite[_orientation+'_car'+randomCar+'.png']);
        tmpCar.origin = rail;
        tmpCar.direction = Math.PI / 6;
        if(_orientation == 'tr'){
            tmpCar.x = rail.x - (Math.cos(tmpCar.direction) * position);
            tmpCar.y = rail.y + (Math.sin(tmpCar.direction) * position);
        }
        else {
            tmpCar.x = rail.x + (Math.cos(tmpCar.direction) * position);
            tmpCar.y = rail.y - (Math.sin(tmpCar.direction) * position);
        }
        tmpCar.randomSpeed = false;
        tmpCar.speed =  tmpCar.randomSpeed ? 1 + Math.random() * 0.9 : rail.speed;

        orderRails[randomRail].push(tmpCar);
        tmpCars.push(tmpCar);
    }
  
    tmpCars = orderRails[0].concat(orderRails[1], orderRails[2], orderRails[3]);
    return tmpCars;
}

function animatedVehicles(_vehicle, _orientation, _worldSize) {
    switch(_orientation)
    {
       case 'tr':
           if(_vehicle.y <= _worldSize.h)
           {
               _vehicle.x -= Math.cos(_vehicle.direction) * _vehicle.speed;
               _vehicle.y += Math.sin(_vehicle.direction) * _vehicle.speed;
           }
           else{
               _vehicle.x = _vehicle.origin.x;
               _vehicle.y = _vehicle.origin.y;
               _vehicle.speed = _vehicle.randomSpeed ? 1 + Math.random() * 0.9 : _vehicle.speed; 
           }
           break;
       case 'bl':
           if(_vehicle.x <= _worldSize.w)
           {
               _vehicle.x += Math.cos(_vehicle.direction) * _vehicle.speed;
               _vehicle.y -= Math.sin(_vehicle.direction) * _vehicle.speed;
           }
           else {
               _vehicle.x = _vehicle.origin.x;
               _vehicle.y = _vehicle.origin.y;
               _vehicle.speed = _vehicle.randomSpeed ? 1 + Math.random() * 0.9 : _vehicle.speed; 
           }
           break;
   }
}


module.exports = { getCars, animatedVehicles };